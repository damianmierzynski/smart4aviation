package com.airline.entity;

import com.airline.entity.enums.Type;
import com.airline.entity.enums.Units;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Cargo entity
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Entity
@Table(name = "cargo")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Cargo implements Serializable {

    @Id
    @Column(name = "cargo_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cargoId;
    @Column(name = "weight")
    private Integer weight;
    @Column(name = "weight_unit")
    @Enumerated(EnumType.STRING)
    private Units weightUnit;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private Type type;
    @Column(name = "pieces")
    private Integer pieces;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Flight.class)
    private Flight flight;

}
