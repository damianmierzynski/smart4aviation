package com.airline.entity;

import com.airline.entity.enums.AirportArrival;
import com.airline.entity.enums.AirportsDeparture;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Flight entity
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Entity
@Table(name = "flight")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Flight implements Serializable {

    @Id
    @Column(name = "flight_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long flightId;
    @Column(name = "flight_number")
    private Integer flightNumber;
    @Column(name = "departure_airport")
    @Enumerated(EnumType.STRING)
    private AirportsDeparture departureAirportIATACode;
    @Column(name = "arrival_airport")
    @Enumerated(EnumType.STRING)
    private AirportArrival arrivalAirportIATACode;
    @Column(name = "departure_date")
    private Timestamp departureDate;
    @OneToMany(
            mappedBy = "flight",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            targetEntity = Cargo.class,
            fetch = FetchType.LAZY
    )
    private List<Cargo> cargos;

}
