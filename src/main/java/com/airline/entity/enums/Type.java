package com.airline.entity.enums;


/**
 * Enum Type
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

public enum Type {
    baggage, cargo
}
