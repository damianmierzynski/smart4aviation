package com.airline.entity.enums;

/**
 * Enum Units
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

public enum Units {
    kg, lb
}
