package com.airline.entity.enums;

/**
 * Enum Airports departure
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

public enum AirportsDeparture {
    SEA, YYZ, YYT, ANC, LAX
}
