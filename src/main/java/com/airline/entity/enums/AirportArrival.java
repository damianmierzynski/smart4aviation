package com.airline.entity.enums;

/**
 * Enum Airports arrival
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

public enum AirportArrival {
    MIT, LEW, GDN, KRK, PPX
}
