package com.airline.repository;

import com.airline.entity.Cargo;
import com.airline.entity.enums.AirportArrival;
import com.airline.entity.enums.AirportsDeparture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Cargo repository
 *
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Long> {
    /**
     * @param nrFlight   - nr flight
     * @param dateFlight - date flight
     * @return cargo list
     */
    List<Cargo> findByFlightFlightIdAndFlightDepartureDate(Long nrFlight, Timestamp dateFlight);

    /**
     * @param airport - nr flight
     * @param date - date flight
     * @return flight departure
     */
    @Query("select count(f) from Flight f where f.departureAirportIATACode=:airport and CAST(f.departureDate as date) =:date")
    Optional<Long> countFlightDeparture(@Param("airport") AirportsDeparture airport, @Param("date") Date date);

    /**
     * @param airport - nr flight
     * @param date - date flight
     * @return flight arrival
     */
    @Query("select count(f) from Flight f where f.arrivalAirportIATACode=:airport and CAST(f.departureDate as date) =:date")
    Optional<Long> countFlightArrival(@Param("airport") AirportArrival airport, @Param("date") Date date);

    /**
     * @param airport - nr flight
     * @param date - date flight
     * @return  sum baggage departure
     */
    @Query("select sum(c.pieces) from Cargo c where c.flight.departureAirportIATACode=:airport and CAST(c.flight.departureDate  as date) =:date")
    Optional<Long> sumFlightBaggageDeparture(@Param("airport") AirportsDeparture airport, @Param("date") Date date);

    /**
     * @param airport - nr flight
     * @param date - date flight
     * @return sum baggage arrival
     */
    @Query("select sum(c.pieces) from Cargo c where c.flight.arrivalAirportIATACode=:airport and CAST(c.flight.departureDate  as date) =:date")
    Optional<Long> sumFlightBaggageArrival(@Param("airport") AirportArrival airport, @Param("date") Date date);

}
