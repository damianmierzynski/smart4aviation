package com.airline.repository;

import com.airline.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Flight repository
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
}
