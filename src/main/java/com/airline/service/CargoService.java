package com.airline.service;

import org.springframework.stereotype.Service;

import java.text.ParseException;

/**
 * Cargo service
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Service
public interface CargoService {
    Double weightCargoFlight(Long nrFlight, String dateFlight) throws ParseException;

    Double weightBaggageFlight(Long nrFlight, String dateFlight) throws ParseException;

    Double weightFlight(Long nrFlight, String dateFlight) throws ParseException;
}
