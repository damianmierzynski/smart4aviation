package com.airline.service.impl;

import com.airline.entity.enums.AirportArrival;
import com.airline.entity.enums.AirportsDeparture;
import com.airline.repository.CargoRepository;
import com.airline.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;

/**
 * Flight service.
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Service
public class FlightServiceImpl implements FlightService {

    private final CargoRepository cargoRepository;
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    /**
     *
     * @param cargoRepository - car repository
     */
    @Autowired
    public FlightServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }


    /**
     *
     * @param airport - symbol airport
     * @param date - date
     * @return - count flight departure
     * @throws ParseException
     * @throws DataAccessException
     */
    @Override
    public Long countFlightDeparture(String airport, String date) throws ParseException, DataAccessException {
        return cargoRepository.countFlightDeparture(AirportsDeparture.valueOf(airport), toDate(date)).orElseThrow(() -> new NoSuchElementException());
    }

    /**
     *
     * @param airport - symbol airport
     * @param date - date
     * @return  - count flight arrival
     * @throws ParseException
     * @throws DataAccessException
     */
    @Override
    public Long countFlightArrival(String airport, String date) throws ParseException, DataAccessException {
        return cargoRepository.countFlightArrival(AirportArrival.valueOf(airport), toDate(date)).orElseThrow(() -> new NoSuchElementException());
    }

    /**
     *
     * @param airport - symbol airport
     * @param date - date
     * @return  - sum baggage departure
     * @throws ParseException
     * @throws DataAccessException
     */
    @Override
    public Long sumFlightBaggageDeparture(String airport, String date) throws ParseException, DataAccessException {
        return cargoRepository.sumFlightBaggageDeparture(AirportsDeparture.valueOf(airport), toDate(date)).orElseThrow(() -> new NoSuchElementException());
    }

    /**
     *
     * @param airport - symbol airport
     * @param date - date
     * @return - sum baggage arrival
     * @throws ParseException
     * @throws DataAccessException
     */
    @Override
    public Long sumFlightBaggageArrival(String airport, String date) throws ParseException, DataAccessException {
        return cargoRepository.sumFlightBaggageArrival(AirportArrival.valueOf(airport), toDate(date)).orElseThrow(() -> new NoSuchElementException());

    }

    /**
     *
     * @param dataString - string data
     * @return - object Date
     * @throws ParseException
     */

    private Date toDate(String dataString) throws ParseException {
        java.util.Date parseDate = format.parse(dataString);
        return new Date(parseDate.getTime());
    }
}
