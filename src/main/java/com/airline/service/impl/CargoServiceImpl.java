package com.airline.service.impl;

import com.airline.entity.enums.Type;
import com.airline.entity.enums.Units;
import com.airline.repository.CargoRepository;
import com.airline.service.CargoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Cargo service.
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@Service
public class CargoServiceImpl implements CargoService {

    private final CargoRepository cargoRepository;
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ssZ");
    private final Double DIVIDEND = 2.2046;

    /**
     *
     * @param cargoRepository - cargo repository
     */
    @Autowired
    public CargoServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    /**
     *
     * @param nrFlight - nr flight
     * @param dateFlight - data flight
     * @return - cargo weight
     * @throws ParseException
     */
    @Override
    public Double weightCargoFlight(Long nrFlight, String dateFlight) throws ParseException {
            return (double) Math.round(cargoRepository.findByFlightFlightIdAndFlightDepartureDate(nrFlight, toTimestamp(dateFlight)).
                    stream().filter(i -> i.getType().equals(Type.cargo)).mapToDouble(i -> {
                if (i.getWeightUnit().equals(Units.lb)) {
                    return i.getWeight() / DIVIDEND;
                }
                return i.getWeight();
            }).sum() * 100) / 100;
    }

    /**
     *
     * @param nrFlight - nr flight
     * @param dateFlight - data flight
     * @return - baggage weight
     * @throws ParseException
     */
    @Override
    public Double weightBaggageFlight(Long nrFlight, String dateFlight) throws ParseException {
        return (double) Math.round(cargoRepository.findByFlightFlightIdAndFlightDepartureDate(nrFlight, toTimestamp(dateFlight)).
                stream().filter(i -> i.getType().equals(Type.baggage)).mapToDouble(i -> {
            if (i.getWeightUnit().equals(Units.lb)) {
                return i.getWeight() / DIVIDEND;
            }
            return i.getWeight();
        }).sum() * 100) / 100;
    }

    /**
     *
     * @param nrFlight - nr flight
     * @param dateFlight - data flight
     * @return - baggage weight
     * @throws ParseException
     */
    @Override
    public Double weightFlight(Long nrFlight, String dateFlight) throws ParseException {
        return (double) Math.round(cargoRepository.findByFlightFlightIdAndFlightDepartureDate(nrFlight, toTimestamp(dateFlight)).
                stream().mapToDouble(i -> {
            if (i.getWeightUnit().equals(Units.lb)) {
                return i.getWeight() / DIVIDEND;
            }
            return i.getWeight();
        }).sum() * 100) / 100;
    }

    /**
     *
     * @param dataString - string data
     * @return - object Timestamp
     * @throws ParseException
     */
    private Timestamp toTimestamp(String dataString) throws ParseException {
        java.util.Date parseDate = format.parse(dataString);
        return new Timestamp(parseDate.getTime());
    }

}
