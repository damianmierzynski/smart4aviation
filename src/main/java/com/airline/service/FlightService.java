package com.airline.service;

import java.text.ParseException;

/**
 * Flight service
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

public interface FlightService {
    Long countFlightDeparture(String airport, String date) throws ParseException;

    Long countFlightArrival(String airport, String date) throws ParseException;

    Long sumFlightBaggageDeparture(String airport, String date) throws ParseException;

    Long sumFlightBaggageArrival(String airport, String date) throws ParseException;
}
