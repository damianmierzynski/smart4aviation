package com.airline.controller;

import com.airline.dto.MessageDTO;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.text.ParseException;

/**
 * Exception Controller
 *
 * @author Damian Mierzynski
 * @since 2021-05-11
 */
@ControllerAdvice
public class ExceptionController {
    /**
     * Exception Handle Controller
     * @param exception - throw exception
     * @return return message on throw
     */
    @ExceptionHandler(value = {ParseException.class , DataAccessException.class , IllegalArgumentException.class })
    public ResponseEntity<MessageDTO> exception(Exception exception) {
        return new ResponseEntity<>(new MessageDTO(exception.getMessage()), HttpStatus.NOT_FOUND);
    }
}
