package com.airline.controller;

import com.airline.service.FlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

/**
 * Flight service
 *
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/api/flight")
public class FlightController {

    private final String COUNT_FLIGHT_DEPARTURE = "/count-flight-departure/{airport}/{dateFlight}";
    private final String COUNT_FLIGHT_ARRIVAL = "/count-flight-arrival/{airport}/{dateFlight}";
    private final String COUNT_BAGGAGE_DEPARTURE = "/count-baggage-departure/{airport}/{dateFlight}";
    private final String COUNT_BAGGAGE_ARRIVAL = "/count-baggage-arrival/{airport}/{dateFlight}";

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightController.class);
    private final FlightService flightService;


    /**
     * @param flightService - flight service
     */
    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }


    /**
     * @param airport    - symbol airport
     * @param dateFlight - date flight
     * @return - count flight departure
     */
    @GetMapping(COUNT_FLIGHT_DEPARTURE)
    public ResponseEntity<Long> countFlightDeparture(@PathVariable(value = "airport") String airport, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- DEPARTURE FLIGHT---");
        return new ResponseEntity<>(flightService.countFlightDeparture(airport, dateFlight), HttpStatus.OK);

    }

    /**
     * @param airport    - symbol airport
     * @param dateFlight - date flight
     * @return - count flight arrvial
     */
    @GetMapping(COUNT_FLIGHT_ARRIVAL)
    public ResponseEntity<Long> countFlightArrival(@PathVariable(value = "airport") String airport, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- ARRIVAL FLIGHT---");
        return new ResponseEntity<>(flightService.countFlightArrival(airport, dateFlight), HttpStatus.OK);
    }

    /**
     * @param airport    - symbol airport
     * @param dateFlight - date flight
     * @return - sum baggage arrival
     */
    @GetMapping(COUNT_BAGGAGE_DEPARTURE)
    public ResponseEntity<Long> sumFlightBaggageDeparture(@PathVariable(value = "airport") String airport, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- ARRIVAL FLIGHT---");
        return new ResponseEntity<>(flightService.sumFlightBaggageDeparture(airport, dateFlight), HttpStatus.OK);
    }

    /**
     * @param airport    - symbol airport
     * @param dateFlight - date flight
     * @return - sum baggage arrival
     */
    @GetMapping(COUNT_BAGGAGE_ARRIVAL)
    public ResponseEntity<Long> sumFlightBaggageArrival(@PathVariable(value = "airport") String airport, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- ARRIVAL FLIGHT---");
        return new ResponseEntity<>(flightService.sumFlightBaggageArrival(airport, dateFlight), HttpStatus.OK);
    }
}
