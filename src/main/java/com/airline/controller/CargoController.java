package com.airline.controller;

import com.airline.service.CargoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

/**
 * Cargo controller
 *
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/api/cargo")
public class CargoController {

    private final String WEIGHT_CARGO = "/weight-cargo/{nrFlight}/{dateFlight}";
    private final String WEIGHT_BAGGAGE = "/weight-baggage/{nrFlight}/{dateFlight}";
    private final String WEIGHT_FLIGHT = "/weight-flight/{nrFlight}/{dateFlight}";

    private static final Logger LOGGER = LoggerFactory.getLogger(CargoController.class);
    private final CargoService cargoService;

    /**
     * @param cargoService - cargo service
     */

    @Autowired
    public CargoController(CargoService cargoService) {
        this.cargoService = cargoService;
    }

    /**
     * @param nrFlight   - nr flight
     * @param dateFlight - data flight
     * @return - weight cargo
     */
    @GetMapping(WEIGHT_CARGO)
    public ResponseEntity<Double> weightCargo(@PathVariable(value = "nrFlight") Long nrFlight, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- WEIGHT CARGO ---");
        return new ResponseEntity<>(cargoService.weightCargoFlight(nrFlight, dateFlight), HttpStatus.OK);
    }

    /**
     * @param nrFlight   - nr flight
     * @param dateFlight - data flight
     * @return - weight baggage
     */
    @GetMapping(WEIGHT_BAGGAGE)
    public ResponseEntity<Double> weightBaggage(@PathVariable(value = "nrFlight") Long nrFlight, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- WEIGHT BAGGAGE ---");
        return new ResponseEntity<>(cargoService.weightBaggageFlight(nrFlight, dateFlight), HttpStatus.OK);
    }

    /**
     * @param nrFlight   - nr flight
     * @param dateFlight - data flight
     * @return - weight flight
     */
    @GetMapping(WEIGHT_FLIGHT)
    public ResponseEntity<Double> weightFlight(@PathVariable(value = "nrFlight") Long nrFlight, @PathVariable(value = "dateFlight") String dateFlight) throws ParseException {
        LOGGER.info("--- WEIGHT FLIGHT ---");
        return new ResponseEntity<>(cargoService.weightFlight(nrFlight, dateFlight), HttpStatus.OK);
    }

}
