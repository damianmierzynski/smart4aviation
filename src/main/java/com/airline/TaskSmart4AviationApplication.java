package com.airline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot microservice.
 * @author Damian Mierzynski
 * @since 2021-05-11
 */
@SpringBootApplication
public class TaskSmart4AviationApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskSmart4AviationApplication.class, args);
	}

}
