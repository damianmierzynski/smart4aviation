package com.airline.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Dto Message
 *
 * @author Damian Mierzynski
 * @since 2021-05-11
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class MessageDTO implements Serializable {
    private String message;
}
